const webpack = require('webpack');
const path = require('path');

module.exports = {
    devtool: 'cheap-source-map',
    entry:   [
        path.resolve(__dirname, 'src/index.js'),
    ],
    output: {
        path:       path.resolve(__dirname, 'dist'),
        publicPath: '/',
        filename:   './app-bundle.min.js',
    },
    module: {
        rules: [
            { test: /\.css$/, include: path.resolve(__dirname, 'src') },
            {
                test:    /\.js[x]?$/,
                include: path.resolve(__dirname, 'src'),
                exclude: /node_modules/,
                loader:  'babel-loader',
            },
            {
                test: /\.scss$/,
                use:  ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
            },
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
    ],
};
