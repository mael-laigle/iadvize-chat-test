import get from 'lodash/get';
import * as ActionTypes from '../actions';

const chat = (state = {}, action) => {
    switch (action.type) {
    case ActionTypes.PUBLISH_MESSAGE:
        return {
            ...state,
            [action.chatId]: {
                ...get(state, action.chatId, {}),
                messages: [...get(state, [ action.chatId, 'messages'], []), action.message],
            },
        };
    case ActionTypes.TYPING:
        return {
            ...state,
            [action.chatId]: {
                ...get(state, action.chatId, {}),
                typings: {
                    ...get(state, [action.chatId, 'typings'], {}),
                    [action.clientId]: action.isTyping,
                },
            },
        };
    default:
        return state;
    }
};

export default chat;
