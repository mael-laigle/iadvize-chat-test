import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import chat from '../reducers';

const configureStore = () => createStore(
    chat,
    applyMiddleware(thunkMiddleware),
);

export default configureStore;
