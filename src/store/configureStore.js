import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import chat from '../reducers';

const loggerMiddleware = createLogger();

const configureStore = () => createStore(
    chat,
    applyMiddleware(thunkMiddleware, loggerMiddleware),
);

export default configureStore;
