import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/chat-container.scss';

const ChatContainer = ({ className, item, ...other }) => (
    <div className={classNames('chat-container', className, { 'chat-container--item': item })} {...other} />
);

ChatContainer.propTypes = {
    className: PropTypes.string,
    item:      PropTypes.bool,
};

export default ChatContainer;
