import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/button.scss';

const Button = ({
    className, children, disabled, onClick, ...others
}) => (
    <button
        type="button"
        className={classNames('btn', className, {
            'btn--disabled': disabled,
        })}
        onClick={disabled ? null : onClick}
        {...others}
    >
        <span>
            {children}
        </span>
    </button>
);

Button.propTypes = {
    className: PropTypes.string,
    onClick:   PropTypes.func,
    disabled:  PropTypes.bool,
    children:  PropTypes.node.isRequired,
};

Button.defaultProps = {
    disabled: false,
};


export default Button;
