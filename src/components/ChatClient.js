import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import last from 'lodash/last';
import get from 'lodash/get';
import TextField from './TextField';
import Button from './Button';
import MessageList from './MessageList';
import '../styles/chat-client.scss';

export default class ChatClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: props.messages,
            text:     '', // fully controlled input
        };
        this.handleMessageSubmit = this.handleMessageSubmit.bind(this);
        this.handleTyping = this.handleTyping.bind(this);
        this.handleStopTyping = this.handleStopTyping.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleEnterPress = this.handleEnterPress.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        // Use lodash last in place of props.messages[props.messages.length - 1]... improve readability;
        // Use lodash get in place of
        // const propsLastMessage = last(props.message);
        // const stateLastMessage = last(state.message);
        // propsLastMessage && stateLastMessage && propsLastMessage.uuid !== stateLastMessage.uuid...
        if (state.messages.length === 0
            || get(last(props.messages), 'uuid', null) !== get(last(state.messages), 'uuid', null)) {
            return {
                messages: [...props.messages],
            };
        }
        return null;
    }

    handleMessageSubmit() {
        const { id, onMessageSubmit, onTyping } = this.props;
        const { text } = this.state;
        const message = {
            uuid:      Date.now(),
            content:   text,
            from:      id,
            createdAt: Date.now(),
        };
        this.setState(prevState => ({
            messages: [...prevState.messages, message],
            text:     '',
        }), () => {
            if (onTyping) {
                onTyping(false, id);
            }
            onMessageSubmit(message);
        });
    }

    handleChange(e) {
        this.setState({
            text: e.target.value,
        });
    }

    handleEnterPress(e) {
        if (e.keyCode === 13) {
            this.handleMessageSubmit();
        }
    }

    handleTyping() {
        const { onTyping, id } = this.props;

        if (onTyping) {
            onTyping(true, id);
        }
    }

    handleStopTyping() {
        /*  todo: factorise stop and type func    */
        const { onTyping, id } = this.props;
        if (onTyping) {
            onTyping(false, id);
        }
    }

    render() {
        const { id, className, typing } = this.props;
        const { text, messages } = this.state;
        return (
            <div className={classNames('chat-client', className)}>
                <h1>
                    {id}
                </h1>
                <MessageList typing={typing} messages={messages} clientId={id} />
                <div className="chat-client-control">
                    <TextField
                        value={text}
                        onBlur={this.handleStopTyping}
                        onFocus={this.handleTyping}
                        onChange={this.handleChange}
                        onKeyDown={this.handleEnterPress}
                    />
                    <Button className="btn--input" onClick={this.handleMessageSubmit}>
                        Send
                    </Button>
                </div>
            </div>
        );
    }
}

ChatClient.propTypes = {
    id:              PropTypes.string.isRequired, // rename it as name ? maybe maybe
    messages:        PropTypes.array,
    onMessageSubmit: PropTypes.func.isRequired,
    onTyping:        PropTypes.func,
    chat:            PropTypes.object,
    className:       PropTypes.string,
    typing:          PropTypes.bool,
};

ChatClient.defaultProps = {
    messages: [],
};
