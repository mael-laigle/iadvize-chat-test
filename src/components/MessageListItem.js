import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/message-list-item.scss';

const MessageListItem = ({
    className,
    right,
    bottom,
    ...others
}) => (
    <li
        className={classNames('message-list-item', {
            'message-list-item--right':  right,
            'message-list-item--bottom': bottom,
        })}
        {...others}
    />
);

MessageListItem.propTypes = {
    messages:  PropTypes.array,
    right:     PropTypes.bool,
    bottom:    PropTypes.bool,
};

MessageListItem.defaultProps = {
    messages: [],
};

export default MessageListItem;
