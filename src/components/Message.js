import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/message.scss';

const Message = ({ message, isYours, className }) => (
    <div className={classNames('message', className, { 'message--yours': isYours })}>
        {message.content}
        {/* Add more data like date... */}
    </div>
);

Message.propTypes = {
    message:   PropTypes.object.isRequired,
    className: PropTypes.string,
    isYours:   PropTypes.bool,
};

export default Message;
