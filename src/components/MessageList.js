import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import MessageListItem from './MessageListItem';
import Message from './Message';
import Paper from './Paper';
import TypingMessage from './TypingMessage';
import '../styles/message-list.scss';

export default class MessageList extends Component {
    constructor(props) {
        super(props);
        this.container = null;
        this.handleScrollToBottom = this.handleScrollToBottom.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        const {
            messages, clientId, className, typing,
        } = this.props;
        return nextProps.messages.length !== messages.length
            || clientId !== nextProps.clientId || nextProps.className !== className || typing !== nextProps.typing;
    }

    componentDidUpdate() {
        this.handleScrollToBottom();
    }

    handleScrollToBottom() {
        if (this.container) {
            this.container.scrollTo(0, this.container.scrollHeight);
        }
    }

    render() {
        const { messages, clientId, typing } = this.props;
        return (
            <Paper className="no-padding-top no-padding-bottom">
                <div className="message-list-container">
                    <ul className={classNames('message-list', classNames)} ref={ref => (this.container = ref)}>
                        {messages && messages.map(message => (
                            <MessageListItem right={clientId === message.from} key={message.uuid}>
                                <Message isYours={clientId === message.from} message={message} />
                            </MessageListItem>
                        ))}
                        {typing && (
                            <MessageListItem bottom>
                                <TypingMessage />
                            </MessageListItem>
                        )}
                    </ul>
                </div>
            </Paper>
        );
    }
}

MessageList.propTypes = {
    clientId:  PropTypes.string.isRequired,
    messages:  PropTypes.array,
    className: PropTypes.string,
    typing:    PropTypes.bool,
};

MessageList.defaultProps = {
    messages: [],
};
