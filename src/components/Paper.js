import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/paper.scss';

const Paper = ({ className, ...other }) => (
    <div className={classNames('paper', className)} {...other} />
);

Paper.propTypes = {
    className: PropTypes.string,
};

export default Paper;
