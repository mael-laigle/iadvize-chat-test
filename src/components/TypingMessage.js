import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Message from './Message';
import '../styles/message.scss';
import typing from '../../images/typing.svg';

const TypingMessage = ({ className }) => (
    <Message
        className={classNames('message--typing', className)}
        message={{
            content: (<img src={typing} alt="•••" />),
        }}
    />
);

TypingMessage.propTypes = {
    className: PropTypes.string,
};

export default TypingMessage;
