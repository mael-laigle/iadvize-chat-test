import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/input.scss';


const TextField = ({ className, disabled, ...others }) => (
    <input
        type="text"
        className={classNames('input', className, {
            'input--disabled': disabled,
        })}
        {...others}
    />
);

TextField.propTypes = {
    className:   PropTypes.string,
    disabled:    PropTypes.bool,
    placeholder: PropTypes.string,

};

TextField.defaultProps = {
    disabled: false,
};


export default TextField;
