import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store';
import Chat from './Chat';
import '../styles/app.scss';

const store = configureStore();

const App = () => (
    <Provider store={store}>
        <Chat id="iadvize-test" clients={['Chat A', 'Chat B']} />
    </Provider>
);

export default App;
