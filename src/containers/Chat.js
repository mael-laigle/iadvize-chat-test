import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import get from 'lodash/get';
import omit from 'lodash/omit';
import publish, { typing } from '../actions';
import ChatClient from '../components/ChatClient';
import ChatContainer from '../components/ChatContainer';

const Chat = ({
    id,
    clients,
    messages,
    typings,
    onMessageSubmit,
    onTyping,
}) => (
    <ChatContainer>
        {clients.map(clientId => (
            <ChatContainer key={clientId} item>
                <ChatClient
                    key={clientId}
                    id={clientId}
                    onMessageSubmit={onMessageSubmit(id)}
                    messages={messages}
                    onTyping={onTyping(id)}
                    typing={Object.values(omit(typings, clientId)).reduce((a, c) => a || c, false)}
                />
            </ChatContainer>
        ))}
    </ChatContainer>
);

Chat.propTypes = {
    id:              PropTypes.string.isRequired,
    clients:         PropTypes.array,
    onMessageSubmit: PropTypes.func.isRequired,
    dispatch:        PropTypes.func.isRequired,
    messages:        PropTypes.array,
    typings:         PropTypes.object,
};

Chat.defaultProps = {
    messages: [],
    typings:  {},
};

const mapDispatchToProps = dispatch => ({
    onMessageSubmit: id => message => dispatch(publish(message, id)), // publish/store message on submit
    onTyping:        id => (isTyping, clientId) => dispatch(typing(id, clientId, isTyping)),
    dispatch,
});

const mapStateToProps = (state, ownProps) =>({ ...get(state, ownProps.id) });// get chat info stored by chat id

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
