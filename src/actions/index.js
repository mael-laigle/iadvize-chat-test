export const PUBLISH_MESSAGE = 'PUBLISH_MESSAGE';

const publish = (message, chatId) => ({
    type: PUBLISH_MESSAGE,
    message,
    chatId,
});

export default publish;

export const TYPING = 'TYPING';

export const typing = (chatId, clientId, isTyping) => ({
    type: TYPING,
    chatId,
    clientId,
    isTyping,
});
