const path = require('path');

module.exports = {
    devtool: 'source-map',
    entry:   ['babel-polyfill', './src/index.js'],
    module:  {
        rules: [
            {
                test:    /\.js?$/,
                exclude: /node_modules/,
                loader:  'babel-loader',
                query:   {
                    presets: ['es2015', 'react', 'stage-3'],
                },
            },
            {
                test: /\.scss$/,
                use:  ['style-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.svg$/,
                use:  'file-loader',
            },
        ],
    },
    output: {
        path:     path.join(__dirname, 'build'),
        filename: 'app-bundle.js',
    },
};
